#!/bin/python3
# -*- coding: utf-8 -*-

'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

import numpy as np


def encontrar_constante(archivo):
    fila = archivo.readline()
    constante_red_bohr = fila.split()[1]
    return float(constante_red_bohr)


def bohr2angstrom(constante_red_bohr):
    factor = 1.889725989
    constante_red_angs = constante_red_bohr * factor
    return constante_red_angs


def especie(archivo):
    archivo.seek(0)
    filas = archivo.readlines()

    cont1 = 0
    especie = []

    cont2 = 0
    cantidad = []

    for j in filas:
        cont1 = cont1 + 1
        if cont1 == 6:
            especie.append(j)
    tipo_atomo = ''.join(especie).split()
    numero_especie = int(len(tipo_atomo))

    for j in filas:
        cont2 = cont2 + 1
        if cont2 == 7:
            cantidad.append(j)
    cantidad_tipo_atomo = ''.join(cantidad).split()

    return tipo_atomo, numero_especie, cantidad_tipo_atomo


def buscar_numero_lista_posiciones(archivo):
    archivo.seek(0)
    filas = archivo.readlines()

    inicio = False
    lista = []

    for i in filas:
        if i == 'Direct\n':
            inicio = True
        if inicio == True:
            lista.append(i)

    lista.remove('Direct\n')
    lista_posiciones = ''.join(lista).split('\n')
    lista_posiciones = [x for x in lista_posiciones if x != '']
    numero_lista_posiciones = int(len(lista_posiciones))

    return lista_posiciones, numero_lista_posiciones


def cell_parameters(archivo, constante_red_bohr):
    archivo.seek(0)
    filas = archivo.readlines()

    matrix = []
    super_cell = [0, 0, 0]

    cont = 0
    for j in filas:
        cont = cont + 1
        if cont >= 3 and cont <= 5:
            matrix.append(j)

    for i in range(0, 3):
        super_cell[i] = ''.join(matrix).split('\n')[i].strip()

    a = np.empty((3, 3))
    for m in range(0, 3):
        for n in range(0, 3):
            a[m][n] = super_cell[m].split()[n]

    alat = a / constante_red_bohr

    return alat


def escribir_inputfile(constante_red_angs, numero_lista_posiciones, numero_especie, tipo_atomo, cantidad_tipo_atomo, lista_posiciones, alat):
    _escribir_control()
    _escribir_system(constante_red_angs,
                     numero_lista_posiciones, numero_especie)
    _escribir_electrons()
    _escribir_ions()
    _escribir_atomic_especies(tipo_atomo, numero_especie)
    _escribir_atomic_positions(numero_especie, cantidad_tipo_atomo,
                               tipo_atomo, lista_posiciones, numero_lista_posiciones)
    _escribir_K_POINTS()
    _escribir_cell_parameters(alat)


def _escribir_control():
    with open('input.in', 'w') as f:
        f.write('''
&CONTROL
	            title = 'Input file' ,
	      calculation = 'scf' ,
             restart_mode = 'from_scratch' ,
	       pseudo_dir = './' ,
                   outdir = './tmp' ,
	           prefix = 'pseudo' ,
	          tstress = .true. ,
                  tprnfor = .true. ,''')


def _escribir_system(constante_red_angs, numero_lista_posiciones, numero_especie):
    with open('input.in', 'a+') as f:
        f.write('''
/
&SYSTEM
                    ibrav = 0 ,
	        celldm(1) = {alpha_qe} ,
                      nat = {num_at} ,
                     ntyp = {tipo_at} ,
                     nbnd = 300 ,
                  ecutwfc = 50.0 ,
                  ecutrho = 500.0 ,
              occupations = 'smearing' ,
                 smearing = 'marzari-vanderbilt' ,
                  degauss = 0.0075 ,
               tot_charge = 0.000000 ,
starting_magnetization(1) = +0.5 ,
starting_magnetization(2) = +0.5 ,
starting_magnetization(3) = +0.0 ,
                london_s6 = 0.750 ,
              london_rcut = 50.0 ,
                 noncolin = .true. ,
               lda_plus_u = .false. ,
                 lspinorb = .true. ,
                    nosym = .true. ,'''.format(alpha_qe=constante_red_angs, num_at=numero_lista_posiciones, tipo_at=numero_especie))


def _escribir_electrons():
    with open('input.in', 'a+') as f:
        f.write('''
/
&ELECTRONS
         electron_maxstep = 200 ,
                 conv_thr = 1.0D-7 ,
              startingwfc = 'random' ,
              startingpot = 'atomic' ,
              mixing_mode = 'local-TF' ,
              mixing_beta = 0.50 ,
              mixing_ndim = 24 ,
          diagonalization = 'cg ' ,
         diago_cg_maxiter = 24 ,
           diago_full_acc = .false. ,''')


def _escribir_ions():
    with open('input.in', 'a+') as f:
        f.write('''
/
&IONS
             ion_dynamics = 'bfgs' ,
        wfc_extrapolation = 'atomic' ,
        pot_extrapolation = 'atomic' ,
                  upscale = 100.0D0 ,
                bfgs_ndim = 4 ,
         trust_radius_max = 0.5D0 ,
         trust_radius_min = 1.0D-3 ,
         trust_radius_ini = 0.25D0 ,''')


def _escribir_atomic_especies(tipo_atomo, numero_especie):
    with open('input.in', 'a+') as f:
        f.write('''
/
ATOMIC_SPECIES''')

    dic_elementos = {'H': 1.0079,
                     'He': 4.0026,
                     'Li': 6.941,
                     'Be': 9.0122,
                     'B': 10.811,
                     'C': 12.0107,
                     'N': 14.0067,
                     'O': 15.9994,
                     'F': 18.9984,
                     'Ne': 20.1797,
                     'Na': 22.9897,
                     'Mg': 24.305,
                     'Al': 26.9815,
                     'Si': 28.0855,
                     'P': 30.9738,
                     'S': 32.065,
                     'Cl': 35.453,
                     'K': 39.0983,
                     'Ar': 39.948,
                     'Ca': 40.078,
                     'Sc': 44.9559,
                     'Ti': 47.867,
                     'V': 50.9415,
                     'Cr': 51.9961,
                     'Mn': 54.938,
                     'Fe': 55.845,
                     'Ni': 58.6934,
                     'Co': 58.9332,
                     'Cu': 63.546,
                     'Zn': 65.39,
                     'Ga': 69.723,
                     'Ge': 72.64,
                     'As': 74.9216,
                     'Se': 78.96,
                     'Br': 79.904,
                     'Kr': 83.8,
                     'Rb': 85.4678,
                     'Sr': 87.62,
                     'Y': 88.9059,
                     'Zr': 91.224,
                     'Nb': 92.9064,
                     'Mo': 95.94,
                     'Tc': 98,
                     'Ru': 101.07,
                     'Rh': 102.9055,
                     'Pd': 106.42,
                     'Ag': 107.8682,
                     'Cd': 112.411,
                     'In': 114.818,
                     'Sn': 118.71,
                     'Sb': 121.76,
                     'I': 126.9045,
                     'Te': 127.6,
                     'Xe': 131.293,
                     'Cs': 132.9055,
                     'Ba': 137.327,
                     'La': 138.9055,
                     'Ce': 140.116,
                     'Pr': 140.9077,
                     'Nd': 144.24,
                     'Pm': 145,
                     'Sm': 150.36,
                     'Eu': 151.964,
                     'Gd': 157.25,
                     'Tb': 158.9253,
                     'Dy': 162.5,
                     'Ho': 164.9303,
                     'Er': 167.259,
                     'Tm': 168.9342,
                     'Yb': 173.04,
                     'Lu': 174.967,
                     'Hf': 178.49,
                     'Ta': 180.9479,
                     'W': 183.84,
                     'Re': 186.207,
                     'Os': 190.23,
                     'Ir': 192.217,
                     'Pt': 195.078,
                     'Au': 196.9665,
                     'Hg': 200.59,
                     'Tl': 204.3833,
                     'Pb': 207.2,
                     'Bi': 208.9804,
                     'Po': 209,
                     'At': 210,
                     'Rn': 222,
                     'Fr': 223,
                     'Ra': 226,
                     'Ac': 227,
                     'Pa': 231.0359,
                     'Th': 232.0381,
                     'Np': 237,
                     'U': 238.0289,
                     'Am': 243,
                     'Pu': 244,
                     'Cm': 247,
                     'Bk': 247,
                     'Cf': 251,
                     'Es': 252,
                     'Fm': 257,
                     'Md': 258,
                     'No': 259,
                     'Rf': 261,
                     'Lr': 262,
                     'Db': 262,
                     'Bh': 264,
                     'Sg': 266,
                     'Mt': 268,
                     'Hs': 277,
                     }

    with open('input.in', 'a+') as f:
        for i in range(0, numero_especie):
            f.write('\n {atomo}  {masa_atomica} pseudo.UPF'.format(
                atomo=tipo_atomo[i], masa_atomica=dic_elementos[tipo_atomo[i]]))


def _escribir_atomic_positions(numero_especie, cantidad_tipo_atomo, tipo_atomo, lista_posiciones, numero_lista_posiciones):
    atomos = []
    for i in range(0, numero_especie):
        for j in range(0, int(cantidad_tipo_atomo[i])):
            atomos.append(tipo_atomo[i])

    posiciones = [[]] * numero_lista_posiciones
    for i in range(0, numero_lista_posiciones):
        posiciones[i] = atomos[i].strip().split()

    for i in range(0, numero_lista_posiciones):
        posiciones[i].append(lista_posiciones[i])

    atomos_coordenadas = [[]] * numero_lista_posiciones
    for i in range(0, numero_lista_posiciones):
        atomos_coordenadas[i] = "".join(posiciones[i])

    with open('input.in', 'a+') as f:
        f.write('\nATOMIC_POSITIONS {crystal}\n')
        for i in range(0, numero_lista_posiciones):
            f.write(' {coordenada}\n'.format(coordenada=atomos_coordenadas[i]))


def _escribir_K_POINTS():
    with open('input.in', 'a+') as f:
        f.write('''
K_POINTS {automatic}
  1 1 1 0 0 0''')


def _escribir_cell_parameters(alat):
    alat_list = alat.tolist()
    matrix_alat = [[]] * 3
    with open('input.in', 'a+') as f:
        f.write('\nCELL_PARAMETERS {alat}\n')
        for i in range(0, 3):
            matrix_alat[i] = " ".join(map(str, alat_list[i]))
            f.write(' {alat_posiciones}\n'.format(
                alat_posiciones=matrix_alat[i]))


def main():
    with open('POSCAR') as archivo:
        constante_red_bohr = encontrar_constante(archivo)
        constante_red_angs = bohr2angstrom(constante_red_bohr)
        tipo_atomo, numero_especie, cantidad_tipo_atomo = especie(archivo)
        lista_posiciones, numero_lista_posiciones = buscar_numero_lista_posiciones(
            archivo)
        alat = cell_parameters(archivo, constante_red_bohr)

    escribir_inputfile(constante_red_angs, numero_lista_posiciones,
                       numero_especie, tipo_atomo, cantidad_tipo_atomo, lista_posiciones, alat)

if __name__ == '__main__':
    main()
