#!/bin/bash

# Escrito por Diego Andrés Restrepo Leal
# diegorestrepoleal@gmail.com

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo
echo "$VERDE Convert POSCAR to inpufile QE $LIMPIAR"
echo

echo
echo "$LILA Check if numpy is installed $LIMPIAR"
echo
cat > check_numpy.py << EOF
try:
	import numpy
	print("numpy yes installed")
except ImportError:
	print("numpy not installed")
EOF
autopep8 -i check_numpy.py
python3 check_numpy.py

echo
echo "$AZUL If numpy is $LIMPIAR not installed $AZUL, you must install it for POSCAR2QE.py to work. $LIMPIAR"
echo "$AZUL One option is to install numpy with pip3 $LIMPIAR ($LILA pip3 install numpy $LIMPIAR) $AZUL, but you can use the method you want.$LIMPIAR"
echo

echo
echo "$AZUL Copy POSCAR2QE.py $LIMPIAR"
echo
cp ../POSCAR2QE.py .

echo
echo "$VERDE Run POSCAR2QE.py $LIMPIAR"
echo
python3 POSCAR2QE.py

echo
echo "$ROJO Remove POSCAR2QE.py and check_numpy.py $LIMPIAR"
echo
rm POSCAR2QE.py check_numpy.py
